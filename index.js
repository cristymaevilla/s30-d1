const express= require("express");
// import/require mongoose package:
const mongoose=require("mongoose");

const app = express();
const port =3001;

// connection to mongoDB Atlas
// SYNTAX: mongoose.connect("<MongoDB Atlas connection string>", {useNewUrlParser:true});
mongoose.connect("mongodb+srv://dbcristyvilla:D4pe7npdr9J6r7ct@wdc028-course-booking.ktimy.mongodb.net/b138_to-do?retryWrites=true&w=majority",
	{
		useNewUrlParser:true,
		useUnifiedTopology:true
	}
);

// store the mongoose connection to variable:
let db = mongoose.connection;

// If the connection error occured , this is the output in console:
// console.error.bind = prints error in browser and terminal; "connection error" is the mssg. displayed
db.on("error", console.error.bind(console, "connection error"));

// if the connection is successful, this is the output in console:
db.once("open", ()=> console.log("We're connected to the cloud database"));

// SCHEMA- determines the structure of the documents to be written in the database
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
})

// Create the Task model:
const Task= mongoose.model("Task", taskSchema);


app.use(express.json());

app.use(express.urlencoded({extended:true}));

// Create a POST route to create a new task
app.post("/tasks", (req, res)=> {
	Task.findOne({name: req.body.name}, (err, result)=>{
		// if doc. was found and the doc's name matches the info sent via client/postman
		if(result != null && result.name == req.body.name){
			return res.send("Duplicate task found");
		}
		else{
			let newTask = new Task ({
				name: req.body.name,
			});
			newTask.save((saveErr, savedTask) =>{
				if(saveErr){
					return console.error(saveErr);
				}
				else{
					return res.status(201).send("New task created")
				}
			})
		}
	})

})

// GET request to retrieve all tasks
app.get("/tasks", (req, res)=> {
	Task.find({}, (err, result)=>{
		// if error occurs
		if (err){
			return console.log(err);
		}
		// if no errors are found
		else{
			return res.status(200).json({data:result})
		}
	})
})




//ACTIVITy========================================================================
// 1. Create a User schema.

const userSchema = new mongoose.Schema({
	username: String,
	password:String
});

// 2. Create a User model.
const User= mongoose.model("User", userSchema);

// 3. Create a POST route that will access the "/signup" route that will create a user.

app.post("/signup", (req, res)=> {
	User.findOne({username: req.body.username , password: req.body.password}, (err, result)=>{

		if(result != null && result.username == req.body.username){
			return res.send("Duplicate username found");
		}
		else{
			let newUser = new User ({
				username: req.body.username,
				password: req.body.password
			});
			newUser.save((saveErr, savedTask) =>{
				if(saveErr){
					return console.error(saveErr);
				}
				else{
					return res.send("New user registered")
				}
			})
		}
	})

})

app.listen(port, ()=> console.log (`Server is running at post ${port}`));